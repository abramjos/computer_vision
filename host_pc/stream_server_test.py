 
"""
Created on Tue Dec 1 17:53:14 2017

@author: Abraham Jose (abramjos@gmail.com) 
"""
  

import os.path
import numpy as np
import cv2
import socket
import serial
 
import pandas as pd

class VideoStreamingTest(object):
    def __init__(self):
        '''
        connects rpi and host pc and displays the socket

        '''
        self.server_socket = socket.socket()
        self.server_socket.bind(('10.42.0.1', 8300))
        print "working..."               
        self.server_socket.listen(0)
        self.connection, self.client_address = self.server_socket.accept()
        print "Connection from: ", self.client_address
  
import csv
import pandas as pd


class VideoStreamingTest(object):
    def __init__(self):
        
        self.server_socket = socket.socket()
        self.server_socket.bind(('10.42.0.1', 8300))
        print "working..."        
	
        
        
        self.server_socket.listen(0)
        self.connection, self.client_address = self.server_socket.accept()
        print "Connection from: ", self.client_address
        self.connection = self.connection.makefile('rb')
        self.streaming()
          
        
        
 
     
    def streaming(self): 
        '''
           To stream data from rpi and  Arduino
           1)streams rpi camera data
           2)collects keystroke data from Arduino
           3)saves it to host PC as images and keys.csv
           4)recollects previous data from host pc os.path and attaches new data  
        ''' 
  

    def streaming(self):
         try:
            print "Connection from: ", self.client_address
            print "Streaming..."
            print "Press 'q' to exit"
	    frame = 1
            stream_bytes = ' '
            frames=1
 
            filename ="/home/abraham/project_collect_data/keys.csv"
        
            col = ['speed','direction'] #index for dataframe for collecting keystrokes
            df=pd.DataFrame(columns =col) #creating pandas dataframe
            
            #checks for previous data
  
            filename = "/home/abraham/project_collect_data/keys.csv"
        
            col = ['speed','direction']
            df=pd.DataFrame(columns =col)
            
            if (os.path.isfile(filename) ==True):
                df=pd.DataFrame.from_csv(filename)
                frames=df.index[-1]
                print(df.index[-1])
                print(df)

 
            #Intiate Arduino Host pc connection through port ttyACM0  
            portPath = "/dev/ttyACM0" 
            baud = 9600 
            serial_connection=serial.Serial(portPath, baud)
            
            #to iterate between streamed data and keystrokes until ctrl+c is pressed to terminate 
            inst=True
            while inst:
                
                stream_bytes += self.connection.read(1024)
                #uses start and stop bit of encoded camera data to collect camera stream data
  
            #Arduino
            portPath = "/dev/ttyACM0" 
            baud = 9600 
            s=serial.Serial(portPath, baud)
        
            while True:
                
                stream_bytes += self.connection.read(1024)
                first = stream_bytes.find('\xff\xd8')
                last = stream_bytes.find('\xff\xd9')
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last + 2]
                    stream_bytes = stream_bytes[last + 2:]
 
                    #decodes encoded camera data
                    image = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
                    cv2.imshow('image', image)
                    cv2.imwrite("/home/abraham/project_collect_data/training_image/image-" + str(frames) + ".jpg", image)
                    frames+=1
                    #Collect keyboard data,converts it to panda series and appends to dataframe
                    serial_line = serial_connection.readline()
  
                    image = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
                    #image = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.CV_LOAD_IMAGE_UNCHANGED)
                    cv2.imshow('image', image)
                    #print(image)
                    cv2.imwrite("/home/abraham/project_collect_data/training_images/image-" + str(frames) + ".jpg", image)
                    frames+=1
                    #Collect keyboard data
                    serial_line = s.readline()
                    serial_line = serial_line.replace('\n', ' ').replace('\r', '')
                    data=pd.Series(serial_line.split(","),index=col)
                    data.name=frames
                    df=df.append(data,ignore_index=True)
                    print(data)
 
              	    
        finally:
           
            '''
            prompts if you want to save the newly attached data and closes the python socket
            '''  
  
		    
                 
        finally:
            yes = {'yes','y', 'ye', ''}
            no = {'no','n'}
            choice=raw_input("\n Do you wanna save current data input?(y/n) \t").lower()
            if choice in yes:
                df.to_csv("keys.csv")
                print 'Sucessfully saved last images and keyframes'
            elif choice in no:
                print 'No data saved'
            else:
                sys.stdout.write("Please respond with 'yes' or 'no'")
            
            self.connection.close()
            self.server_socket.close()

if __name__ == '__main__':
 
    VideoStreamingTest()
  
    VideoStreamingTest()
